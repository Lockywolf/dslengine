;;;
;;  A demonstration of the macro expansion using regexp features.
;;  Here we will define a Pascal-like syntax for a very reduced subset of
;;  Scheme, compiling directly into simple Scheme expressions.
;;;

(define-macro (pasqualish s)
   (defparsers
      (letrec
	    ((cutlist (lambda (l)
			   (letrec
				 ((t
				   (lambda (x)
				      (if (null? x) '()
					  (if (null? (cdr x)) '()
					      (cons (car x) (t (cdr x))))))))
			      (t (cdr l)))))
	     (thelexer
	      (regexp
	       (
		((
		  ("function" / "begin" / "end" / "var" /
			      "for" / "while" /
			      "if" / "then" / "else")
		  -> (lambda (l)
			(list
			 (list
			  (string->symbol
			   (string-append "KW-"
					  (list->string (map char-upcase l))))
			  )))))
		/
		((
		  palpha
		  +
		  (((palpha / pdigit) *) =)
		  )
		 -> (mktoken 'IDENT (oxx string->symbol list->string)))
		/
		((
		  ((#\- / #\+) =)
		  +
		  (pdigit *)
		  +
		  (("." + (pdigit *)) =)) -> (mktoken 'NUMERIC list->string))
		/
		((#\" + ((#\" !) *) + #\")
		 ->
		 (mktoken 'STRING (oxx list->string cutlist)))
		/
		("<-" -> (mktokenx 'LET))
		/
		((($ "+-*/^") / "=" / "<=" / ">=" / "<>" / ($ "<>"))
		 ->
		 (mktoken 'OPER (oxx string->symbol list->string)))
		/
		(#\, -> (mktokenx 'COMMA))
		/
		(#\; -> (mktokenx 'SEMI))
		/
		(#\$ -> (mktokenx 'SEMI)) ;;  an alternative semicolon.
		/
		(":=" -> (mktokenx 'SET))
		/
		("(" -> (mktokenx 'LB))
		/
		(")" -> (mktokenx 'RB))
		/
		("[" -> (mktokenx 'LSB))
		/
		("]" -> (mktokenx 'RSB))
		/
		((($ " \t\n") *) **)
		) *)
	      )
	     (theparser
	      (letrec
		    ((arglst
		      (regexp
		       (((IDENT) + (COMMA) + arglst) :-> (list (cons $$0 $2)))
		       /
		       ((IDENT) :-> (list (list $$0)))))
		     (exprlst
		      (regexp
		       ((expr + (COMMA) + exprlst) :-> (list (cons $0 $2)))
		       /
		       (expr :-> (list (list $0)))))
		     (exprxlst
		      (regexp
		       (((IDENT) + (LET) + expr + (SEMI) + exprxlst)
			:-> (list (list (cons 'let (cons (list (list $$0 $2)) $4))))
			)
		       /
		       ((expr + (SEMI) + exprxlst) :-> (list (cons $0 $2)))
		       /
		       ((expr + (SEMI)) :-> (list (list $0)))))
		     (hlexprs
		      (regexp
		       ((hlexpr + hlexprs)
			:-> (list (cons $0 $1)))
		       /
		       (hlexpr :-> (list (list $0)))
		       )
		      )
		     (hlexpr
		      (regexp
		       (((KW-VAR) + arglst + (SEMI))
			:->
			(list (cons 'begin
				    (map
				     (lambda (x)
					(list 'define x 0))
				     $1
				     ))))
		       /
		       (((KW-FUNCTION) + (IDENT) + (LB) + arglst + (RB)
				       + (KW-BEGIN) + exprxlst + (KW-END))
			:->
			(list
			 (cons
			  'define
			  (cons (cons $$1 $3)
				$6)))
			)
		       /
		       (((KW-FUNCTION) + (IDENT) + (LB) + (RB)
				       + (KW-BEGIN) + exprxlst + (KW-END))
			:->
			(list
			 (cons
			  'define
			  (cons (list $$1)
				$5)))
			)
		       /
		       (((KW-BEGIN) + exprxlst + (KW-END))
			:->
			(list (cons 'begin $1)))))
		     (exprt
		      (regexp
		       (((IDENT) + (LB) + exprlst + (RB))
			:->
			(list (cons $$0 $2))
			)
		       /
		       (((KW-FOR) + (LB) + expr + (COMMA) +
			 expr + (COMMA) + expr + (RB) + expr)
			:->
			(let (
			      (nm (gensym))
			      (cnd $4)
			      (stp $6)
			      (bd $8)
			      )
			  (print "FOR!!!!")
			   (list
			    (list 'begin $2
				  `(letrec
					 ((,nm
					   (lambda ()
					      (if ,cnd
						  (begin
						     ,bd
						     ,stp
						     (,nm))
						  '()))))
				      (,nm)))))
			)
		       /
		       (((IDENT) + (LB) + (RB))
			:->
			(list (list $$0))
			)
		       /
		       (((IDENT) + (SET) + expr)
			:->
			(list (list 'set! $$0 $2))
			)
		       /
		       (((KW-IF) + (LB) + expr + (RB) + (KW-THEN) + expr + (KW-ELSE) + expr)
			:->
			(list (list 'if $2 $5 $7))
			)
		       /
		       (((KW-IF) + (LB) + expr + (RB) + (KW-THEN) + expr)
			:->
			(list (list 'if $2 $5))
			)
		       /
		       (((KW-BEGIN) + exprxlst + (KW-END))
			:->
			(list (list 'begin $1))
			)
		       /
		       ((STRING)
			:->
			(list $$0))
		       /
		       ((NUMERIC)
			:->
			(list (string->number $$0)))
		       /
		       ((IDENT)
			:->
			(list $$0))
		       /
		       (((LB) + expr + (RB))
			:->
			(list $1))
		       ))
		     (expr
		      (regexp
		       ((exprt + (OPER) + expr)
			:->
			(list (cons $$1 (list $0 $2))))
		       /
		       (exprt :-> (list $0))
		       ))
		     )
		 (regexp
		  hlexprs
		  :-> (list (cons 'begin $0))
		  ))))
	 (car (result (theparser (result (thelexer (string->list s))))))
	 )))

(define-macro (pasqualish-f s)
   (letrec ((listappend
	     (lambda (l)
		(if (pair? l)
		    (string-append (listappend (car l))
				   "\n"
				   (listappend (cdr l)))
		    (if (string? l) l
			"")))))
      (let* ((t (listappend (with-input-from-file s read-lines))))
	 `(pasqualish ,t))))
