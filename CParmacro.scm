;;
;; The stuff defined in CParsing.scm redefined
;;  specially for macro expanders
;;

(define-macro (defparsers vvv)
   `(let*
	  (
	   (pchar (lambda (f)
		     (parser (lambda (l)
				(let ((c (car l)))
				   (if (char? c)
				       (if (f c)
					   `((RESULT ,c) ,@(cdr l))
					   `((FAIL "char" ,c) ,@l))
				       `((FAIL "non-character" ,c) ,@l)))))))
	   (palpha 
	    (pchar
	     (lambda (x)
		(if (char? x)
		    (or
		     (and (char>=? x #\a) (char<=? x #\z))
		     (and (char>=? x #\A) (char<=? x #\Z))) #f))))
	   (pdigit
	    (pchar
	     (lambda (x)
		(if (char? x)
		    (and (char>=? x #\0) (char<=? x #\9))))))
	   (parse-any
	    (lambda (l) `((RESULT) ,@l)))
	   (parse-nop
	    (parser (lambda (l) `((FAIL "expected") ,@l))))
	   (parse-eof
	    (lambda (l) (if (null? l) `((RESULT)) `((FAIL "eof") ,@l))))
	   (parse-numb
	    (pR
	     (p+
	      (pOR (pcsx-or (#\- #\+))
		   parse-any)
	      (pMANY pdigit)
	      (pOR (p+ (pcharx #\.)
		       (pMANY pdigit))
		   parse-any))
	     (lambda (x) (list (list->string x)))
	     ))
	   (parse-spaces
	    (pR
	     (pMANY
	      (pcsxx-or " \t\n"))
	     (lambda (l) '())))
	   (parse-ident
	    (pR
	     (p+
	      palpha
	      (pOR
	       (pMANY (pOR palpha pdigit
			   (pcsxx-or "_-+*$%#@:<>|")))
	       parse-any
	       ))
	     (oxx list list->string)))
	   (num
	    (parser
	     (lambda (x)
		(if (number? (car x))
		    `((RESULT ,(car x)) ,@(cdr x))
		    `((FAIL) ,@x)))))
	   (lst
	    (parser
	     (lambda (x)
		(if (pair? (car x))
		    `((RESULT ,(car x)) ,@(cdr x))
		    `((FAIL) ,@x)))))
	   )
       ,vvv))



