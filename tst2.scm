
;;; Bigloo module declaration.

(module CTest
   (include "CParsing.scm")
   (include "CParmacro.scm")
   (main main))

(define-macro (tst pr s)
  `(let ((p ',pr)
	 (s (,pr ',(string->list s))))
     (print "Parser: " p)
     (print "Source: " ,s)
     (print "Result: " s)))

(define (main args)
  (tst (regexp ("x" /* "a")) "aaaaaaxaa")
  (tst (regexp ("a" */ "x")) "aaaaaaxaa")
  (tst (regexp (((#\a - #\b)  */ "xa") -> identity)) "aaaaaaxaqa")
  ;; shit case: result's head is cutted out
  (tst (regexp ((((parse-anything ^)  */ 
		  ("xaq" :-> (list 1 "XAQ" 2))) *) -> identity)) 
       "aaaaaaxaqabknqwberkjuxamansldkjhqwexaqaaz")
  )

;;;------------
