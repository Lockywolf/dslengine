
 You can use this library with any Scheme implementation that support
low level macros (define-macro construction).

 Low level features of the DSL Engine library includes the standard
parsing combinators set. The parser should be a function taking a list
of tokens (characters, whatever) as an argument, and returning one of:
((RESULT . processed-result) unrecognized-rest)
((FAIL optional-reason) unrecognized-input)

 The following macros are used to access the parsing result:

MACRO: success? r
 Return #t if the value r, returned by parser, contain the RESULT

MACRO: result r
 Return the result recognized and probably processed by the parser.

MACRO: rest r
 Return the rest of the input which was not recognized by the parser.

 The very basic parsing combinators are follows:

MACRO: p+ p1 p2 ...
 Try the parser p1, if it is successful, try the next parsers on the rest,
and return the successful result only if all parsers are successful.

MACRO: pOR p1 p2 ...
 Try parsers until one returns successful result.

MACRO: pMANY p
 Try parser on the rest of its result as many times as it possible.

MACRO: pR p r
 Try parser p, and if it is successful, apply the function r to the 
result. Otherwise return the unmodified fail structure.

 It is not yet simple to use this very basic macros directly,
so, there is a macro 'regexp' providing a more friendly syntax for
this building blocks:

MACRO: regexp r

 > string

  recognize a specified string of characters from the input stream

 > ( expr )

  use lists to specify the priority of expressions

 > char

  recognize a single character

 > symbol

  just pass a symbol value (must be a valid parser) 

 > char - char

  recognize a character from the specified range

 > $ string

  recognize any of the characters from the string

 > expr + expr

  recognize expressions sequence

 > expr / expr

  try to recognize the first expression, if it fails,
 recognize the second one.

 > expr -> scheme

  apply the scheme expression (one argument lambda) to the result
 of the expr parsing, if it succeeds. The following substitution is applied
 to the scheme expression:

 >> $num 

  Get the value of the num'th element of the resulting list

 >> $$num

  Get the value of the num'th token of the resulting list 

 > expr :-> scheme

  The shorter form for the previous expansion: the lambda form is generated
 automatically. But here you can only access the argument value using the
 $-substitutions.

 > expr *

  Apply the expr as many times as it possible

 > expr =

  Recognize the expression zero or one times.

 > expr !

  Recognize anything but the specified expression.

 > expr **

  Recognize the expression and ignore its result.

  For example, to define a regular expression which recognizes
a floating number in the input string and returns it as a token
XNUMBER containing Scheme number:

(define parse-numr2
   (regexp
    (
     ((#\- / #\+) =) + 
     (pdigit *) +
     (("." + (pdigit *)) =) +
     ((("e" / "E") + ((#\- / #\+) =) + (pdigit *)) =)
     -> (mktoken 'XNUMBER 
          (oxx string->number list->string))
    )))
