(define-macro (brlm s)
   (defparsers
      (letrec
	    ((cutlist (lambda (l)
			   (letrec
				 ((t
				   (lambda (x)
				      (if (null? x) '()
					  (if (null? (cdr x)) '()
					      (cons (car x) (t (cdr x))))))))
			      (t (cdr l)))))
	     (s2l (lambda (s)
		     (with-input-from-string s read)))
	     (brlx
	      (regexp
	       (
		(#\[ + scmx + #\])
		->
		(mktoken 'BRLCODE (oxx list->string cutlist))
		)
	       /
	       (
		(((#\[ / parse-eof) !) *) -> (mktoken 'BRLSTRING list->string))
			    
		)
	       )
	     (thebrl (regexp (brlx *)))
	     (scmx
	      (regexp
	       (
		 (#\" + ((((#\" !) / "\\\"") *)) + #\")
		 /
		 (((#\] / #\") !) *)
		 ) *))
	     (listappend
	      (lambda (l)
		 (if (pair? l)
		     (string-append (listappend (car l))
				    (listappend (cdr l)))
		     (if (string? l) l
			 ""))))
	     (tocode
	      (regexp
	       (
		((BRLCODE) :-> (list $$0))
		/
		((BRLSTRING) :->
			     (list (string-append
				    "\""
				    (string-for-read $$0) "\"")))) *))
	     )
	 (let* ((x (result (tocode (result (thebrl (string->list s))))))
		(xx (s2l (string-append
			  "(brl " (listappend x) ")"))))
	    xx))))

(define-macro (brl . x)
   (if (pair? x)
       (if (pair? (cdr x))
	   `(begin (display ,(car x))
		   (brl ,@(cdr x)))
	   `(display ,(car x)))
       `(display "\n")))

(define-macro (brl-from-file s)
   (letrec ((listappend
	     (lambda (l)
		(if (pair? l)
		    (string-append (listappend (car l))
				   "\n"
				   (listappend (cdr l)))
		    (if (string? l) l
			"")))))
      (let* ((t (listappend (with-input-from-file s read-lines))))
	 `(brlm ,t))))


