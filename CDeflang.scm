
;; A preliminary implementation of the define-language macro.

;
; Language definition should be expanded to the following statements:
; (define-macro LanguageXXX-def ...) ; - the source of the definition.
; (define-macro LanguageXXX-compile ...) ; - the language compiler macro
; (define-macro LanguageXXX-fromfile ...) ; - read it from file
; (define LanguageXXX-compilefun ...) ; - Runtime compiler function
; (define ...) ; - runtime functions.



; Syntax:
; (define-language "LanguageName"
;     (CFuns ...) ;  - functions for compiler
;     (RFuns ...) ; - functions for runtime
;     (Commons ...) ; - functions common for both compiler and runtime
;     (AstSpec ...) ; - Language AST definition
;     (Syntax ...) ; - Parser from source to AST
;     (Codegen ...) ; - different possible codegens from AST to anything
; )

;; Helper macro: letrec*

(define-macro (letrec* a . b)
   (if (null? a) `(begin ,@b)
       `(letrec (,(car a))
	   (letrec* ,(cdr a) ,@b))))

;;
; First we've to define the inheritance functionality. It'd be parametrized,
;  and should be extensible to an arbitrary macro level functionality.

; The generic format for a structure to be inherited would be following:
; (name (args) (static pairs) macro body constructing parametrized pairs)

(define-macro (symstr . v)
    (letrec ((doit (lambda (x)
		      (if (null? x) ""
			  (let ((cx (car x)))
			     (string-append
			      (cond
				 ((number? cx)
				  (number->string cx))
				 ((list? cx)
				  (doit (list (eval (expand cx)))))
				 ((string? cx)
				  cx)
				 ((symbol? cx)
				  (symbol->string cx))
				 (else "XXX"))
			      (doit (cdr x))))))))
       (string->symbol (doit v))))

(define-macro (define-hier nam args . l)
   (if (null? (cdr l))
       (list 'define-macro (list (expand `(symstr HIER- ,nam)))
	     `(quote ,(car l)))
       (list 'define-macro (list (expand `(symstr HIER- ,nam)) args)
	     `(append (quote ,(car l))
		      ,(cdr l)))))

(define-macro (combine names vals)
   (if (not (null? names))
       `(combine ()
		 ,(append
		   (map (lambda (n)
			   (if (list? n)
			       (expand (cons (expand `(symstr HIER- ,(car n)))
					     (cdr n)))
			       (expand (list (expand `(symstr HIER- ,n))))
			       )) names)
		   vals
		   ))
       (let* ((pairz (make-hashtable))
	      (lst '())
	      )
	  (map (lambda (l)
		  (map
		   (lambda (p)
;		      (print p)
		      (if (not (hashtable-contains? pairz (car p)))
			  (set! lst (append lst (list (car p)))))
		      (hashtable-put! pairz (car p) (cdr p)))
		   l)) vals)
	  (map (lambda (n) (cons n (hashtable-get pairz n))) lst)
	  )))

;;
;; Ok, some generic functionality is ready. Now let's define a new,
;;  "better" version of 'define-macro', which will be a workaround for
;;  the stupid Scheme limitation of the macro evaluation environment.
;;

(define-macro (new-macro nameargs . body)
;   (print (caar body))
   (if (and (list? body)
	    (list? (car body))
	    (symbol? (caar body))
	    (eqv? 'inherit (caar body)))
       (let ((inh (expand (list 'combine (cadar body) (cddar body)))))
;	  (print inh)
	  (list 'define-macro nameargs
		(cons 'letrec* (cons inh (cdr body)))))
       `(define-macro ,nameargs ,@body)))

;;; Sample:
;(define-hier tst ()
;   ((a 1)
;    (b 2)
;    (c (lambda (zz)
;	  (* zz b)))
;    ))
;
;(new-macro (tst x) (inherit (tst))
;	   `(,c ,x))

;; Redefinition of defparsers:

(define-hier defparsers ()
   (
    (pchar (lambda (f)
	      (parser (lambda (l)
			 (let ((c (car l)))
			    (if (char? c)
				(if (f c)
				    `((RESULT ,c) ,@(cdr l))
				    `((FAIL "char" ,c) ,@l))
				`((FAIL "non-character" ,c) ,@l)))))))
    (palpha 
     (pchar
      (lambda (x)
	 (if (char? x)
	     (or
	      (and (char>=? x #\a) (char<=? x #\z))
	      (and (char>=? x #\A) (char<=? x #\Z))) #f))))
    (pdigit
     (pchar
      (lambda (x)
	 (if (char? x)
	     (and (char>=? x #\0) (char<=? x #\9))))))
    (parse-any
     (lambda (l) `((RESULT) ,@l)))
    (parse-nop
     (parser (lambda (l) `((FAIL "expected") ,@l))))
    (parse-eof
     (lambda (l) (if (null? l) `((RESULT)) `((FAIL "eof") ,@l))))
    (parse-numb
     (pR
      (p+
       (pOR (pcsx-or (#\- #\+))
	    parse-any)
       (pMANY pdigit)
       (pOR (p+ (pcharx #\.)
		(pMANY pdigit))
	    parse-any))
      (lambda (x) (list (list->string x)))
      ))
    (parse-spaces
     (pR
      (pMANY
       (pcsxx-or " \t\n"))
      (lambda (l) '())))
    (parse-ident
     (pR
      (p+
       palpha
       (pOR
	(pMANY (pOR palpha pdigit
		    (pcsxx-or "_-+*$%#@:<>|")))
	parse-any
	))
      (oxx list list->string)))
    (num
     (parser
      (lambda (x)
	 (if (number? (car x))
	     `((RESULT ,(car x)) ,@(cdr x))
	     `((FAIL) ,@x)))))
    (lst
     (parser
      (lambda (x)
	 (if (pair? (car x))
	     `((RESULT ,(car x)) ,@(cdr x))
	     `((FAIL) ,@x)))))
    ))

;; Next step will be a definition of the algebraic datatypes language.

; The format is following (ML style):
; node def:
;   type NODE = expr
;   type NODE = exprs
; expr:
;   NODE
;   expr * expr
;   expr -> expr
; exprs:
;   NODE of expr
;   exprs OR exprs

; This representation should be converted into the pattern recognizer generator
; macro and a set of constructor macros or functions (dunno yet).

(define-macro (symbolrec r)
   (let ((v (gensym)))
      `(parser
	(lambda (,v)
	   (if (symbol? (car ,v))
	       (,r
		(append (string->list (symbol->string (car ,v))) (cdr ,v)))
	       `((FAIL "symbol" ,(car v)) ,v))))))
 
(define-hier milner ()
   ((nodename parse-ident)
    (exprshn
     (regexp
      (
       ((SCM symbolrec nodename)
	+
	(SCM psym *)
	+
	exprshn)
       :->
       (list (cons 'CARTESIAN (cons $0 $3))))
      /
      ((SCM symbolrec nodename)
	:->
	(list (list 'TAG $0)))
      ))
    (exprshns0
     (regexp
      ((SCM symbolrec nodename)
       +
       (SCM psym of)
       +
       exprshn)
      :->
      (list (list 'OF $0 $3))))
    (exprshns
     (regexp
      (exprshns0 :-> (list (list 'E $0)))
      /
      ((exprshns0 + (SCM psym OR) + exprshns)
       :-> (list (cons $0 $3))
       )))
    (parse-it
     (regexp
      (
       (
        (SCM psym type) +
        (SCM symbolrec nodename) +
        (SCM psym =) +
	(exprshns /
		 expreshn))
       :->
       (list (list 'NODE $1 $3)))))
    ))
	
      

      
      