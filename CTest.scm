
;;; Bigloo module declaration.

(module CTest
   (include "CParsing.scm")
   (include "CParmacro.scm")
   (include "CRev.scm")
   (include "CBrl.scm")
   (main main))

;; Wow!!! 3rd-level macro, using regexp to define a macro-expander!!!
(define-macro (exp1 . v)
   (defparsers
      (letrec
	    ((epr
	      (let
		    ((body
		      (regexp
		       (num :-> $0)
		       /
		       (lst -> (aprs epr)))))
		 (regexp
		  ((body + (SCM psym +) + epr)
		   :-> (list (+ $0 $2)))
		  /
		  ((body + (SCM psym -) + epr)
		   :-> (list (- $0 $2)))
		  /
		  ((body + (SCM psym *) + epr)
		   :-> (list (* $0 $2)))
		  /
		  ((body + (SCM psym /) + epr)
		   :-> (list (/ $0 $2)))
		  / body
		  ))))
	 (car (result (epr v))))))

;;
;; A bit more fun: Sexpr->string->macro.
;;
(define-macro (pasq2 . v)
   (letrec
	 ((strif
	   (lambda (l)
	      (if (null? l) ""
		  (if (list? l)
		      (string-append (strix (car l)) " "
				     (strif (cdr l)))
		      (if (string? l)
			  (string-append
			   "\"" l "\"")
			  (if (symbol? l)
			      (symbol->string l)
			      (string-append l)))))))
	  (strix
	   (lambda (l)
	      (if (list? l)
		  (string-append "(" (strif l) ")")
		  (strif l)
		  ))))
      `(pasqualish ,(strif v))))

(pasqualish
"
 var x,y;
 var z,k;

 function fa(b,c)
 begin
     x := 1;
     bb <- x+x+x+x;
     print(b+c);
     print(x*(x+x));
     print(\"-------------\");
     print(b*bb);
     print(\"-------------\");
 end

 function test()
 begin
   print(fa(10,10));
   print(\"aaaa\");
   print(\"aaaa\");
   print(\"aaaa\");
   print(fac(2));
   print(fac(3));
   print(fac(4));
   print(fac(5));
   print(fac(6));
   print(fac(7));
   print(fac(8));
   print(fac(9));
   print(fac(10));
   print(fac(11));
   print(fac(12));
   print(fac(13));
 end

")

(pasqualish
 "
 function fac(x)
 begin
   if (x > 0) then x*fac(x - 1) else 1;
 end

")

(pasq2
  var xxx,yyy$
  function ttt(vvv)
  begin
    x:=vvv$
    print(vvv*vvv)$
    print("abcde!")$
  end
  )


(define (main argv)
   (print (exp1 2 + ((10 * 5) / 4))) ;; "Partial evaluation", ha-ha!
   (test) ;; refers to the function, defined in pasqualish style.
   (ttt 10)
   (brlm "Shit:=[(+ 2 2)]\n")
   (brl-from-file "tst.brl")
   )

;;;------------