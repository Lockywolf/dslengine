;
; This is a simple wrapper macro over the regexp functionality.
; It provides a way to transparently fetch the data recognized by
; a regular expression into the variables.
;



(define-macro ($r c . v)
 (defparsers
  (letrec 
      (
       (rv
	(lambda (x)
	  (let* 
	      ((tt ((oxx string->list symbol->string) x))
	       (tr (
		    (regexp
		     (#\$ + #\. + ((#\a - #\z) *)) -> 
		     (oxx string->symbol list->string cddr)
		     ) tt)))
	    (if (success? tr) (result tr) #f)
	    )))
       (vars '())
       (chkcode
	(lambda (l)
	  (cond 
	   ((null? l) '())
	   ((symbol? l)
	    (let ((tv (rv l)))
	      (if tv 
		  (begin
		    (set! vars (cons tv vars))
		    `(lambda (l) (set! ,tv (list->string l)) '()))
		  l)))
	   ((pair? l) (cons (chkcode (car l)) (chkcode (cdr l))))
	   (else l))))
       (mkvlst
	(lambda (l)
	  (if (null? l) '()
	      (cons `(,(car l) '()) (mkvlst (cdr l))))))
       )
    (let* (
	   (nc (chkcode c))
	   (sr (gensym))
	   )
      `(lambda (,sr)
	 (let ,(mkvlst vars)
	   ((regexp ,@nc) (string->list ,sr))
	   ,@v))))))


