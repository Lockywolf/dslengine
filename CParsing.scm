
;;;
;; Some code which probably will be useful.
;;;

;;; * Partial function application. Generic (n-args)...
(define-macro (part f n . args)
   (letrec ((mkargl (lambda (i)
                       (if (> i 0) (cons (gensym) (mkargl (- i 1)))
                           '()))))
      (let ((argz (mkargl n)))
         `(lambda (,@argz) (,f ,@args ,@argz)))))

;;; * Partial function application. One arg left
(define-macro (part1 f . args)
   `(part ,f 1 ,@args))

;;; ** Usage example:

; (define plus1 (part1 + 1))
; (plus1 1) ==> 2

; (define mapdbl (part1 map (part1 * 2)))
; (mapdbl '(1 2 3 4)) ==> (2 4 6 8)

;;; * Partial function application. Two args left.
(define-macro (part2 f . args)
   (let ((arg1 (gensym))
         (arg2 (gensym))
         )
      `(lambda (,arg1 ,arg2) (,f ,@args ,arg1 ,arg2))))

;;; * Partial function application. Three args left.
(define-macro (part3 f . args)
   (let ((arg1 (gensym))
         (arg2 (gensym))
         (arg3 (gensym))
         )
      `(lambda (,arg1 ,arg2 ,arg3) (,f ,@args ,arg1 ,arg2 ,arg3))))

;;; * Partial function application. Four args left.
(define-macro (part4 f . args)
   (let ((arg1 (gensym))
         (arg2 (gensym))
         (arg3 (gensym))
         (arg4 (gensym))
         )
      `(lambda (,arg1 ,arg2 ,arg3 ,arg4)
          (,f ,@args ,arg1 ,arg2 ,arg3 ,arg4))))

;;; * Functional product wih fixed args.

(define-macro (o f g . args)
   `(,f (,g ,@args)))

;;; * A more useful form: functional product with a partial application of
;;;   the specified list of arguments.

(define-macro (ox f g . al)
   `(lambda (,@al) (,f (,g ,@al))))

(define-macro (oxx . al)
   (letrec ((z (lambda (l) (if (null? l) 'x
			       `(,(car l) ,(z (cdr l)))))))
      `(lambda (x) ,(z al))))

;;;
;;  Parsing basics.
;;;

; The result of any recognizer should be one of:
; ((FAIL reason) rest)
; ((RESULT res) rest)

; Test if the recognizer succeded. Implemented as a macro for
; better performance.
(define-macro (success? r)
  `(not (eq? (caar ,r) 'FAIL)))

; Return a stripped result, without testing, is it really result or a failure
; condition.
(define-macro (result r)
   `(cdar ,r))

; Return the rest of the input stream.
(define-macro (rest r)
   `(cdr ,r))

; Wrapper for a parsers - check the EOF condition before applying recognizer.
(define-macro (parser p)
   `(lambda (l)
       (if (null? l) '((FAIL "EMPTY"))
	   (if (procedure? l) (,p (l)) ;; lazy tail
	       (,p l)))))

; An internal implementation for a "+" combinator, should not be used.
(define-macro (p+0 p1 p2)
   `(lambda (l)
       (let ((r1 (,p1 l)))
	  (if (success? r1)
	      (let ((r2 (,p2 (rest r1))))
		 (if (success? r2)
		     (cons (cons 'RESULT (append (result r1) (result r2)))
			   (rest r2))
		     (cons (list 'FAIL "p+" (car r2)) l)))
	      r1
	      ))))


; A helper macro used to implement a nested applications loops:
; (m p1 (m p2 ... (m px pn)))
(define-macro (pselect m p1 . po)
   (if (null? po) p1
       (let ((p2 (car po))
	     (px (cdr po)))
	  `(,m ,p1 (pselect ,m ,p2 ,@px)))))

; A nested application of the p+0 combinator.
(define-macro (p+ p1 . po)
   `(pselect p+0 ,p1 ,@po))

; An internal OR combinator implementation.
(define-macro (pOR0 p1 p2)
   `(lambda (l)
       (let ((r1 (,p1 l)))
	  (if (success? r1)
	      r1
	      (,p2 l)))))

; Nested application of the pOR0 combinator.
(define-macro (pOR p1 . po)
   `(pselect pOR0 ,p1 ,@po))

; One-and-some combinator implementation.
(define-macro (pMANY p)
   (let ((nm (gensym)))
      `(letrec
	     ((,nm
	       (lambda (pr l)
		  (let ((r (,p l)))
		     (if (success? r)
			 (,nm (append pr (result r)) (rest r))
			 (if (null? pr)
			     r
			     (cons (cons 'RESULT pr) l)
			     ))))))
	  (part1 ,nm '())
	  )))

; OR-MANY combinator needed to simulate LR parsers

(define-macro (pORMANY p1 p2)
   (let ((nm (gensym)))
      `(letrec
	     ((,nm
	       (lambda (pr l)
		  (let ((r0 (,p1 l)))
		     (if (success? r0)
			 (cons (append pr (result r0)) (rest r0))
			 (let ((r (,p2 l)))
			    (if (success? r)
				(,nm (append pr (result r)) (rest r))
				(if (null? pr)
				    r
				    (cons (cons 'RESULT pr) l)
				    ))))))))
	  (part1 ,nm '())
	  )))
	  

; Apply-to-the-recognition-result combinator.
(define-macro (pR p r)
   `(lambda (l)
       (let ((rs (,p l)))
	  (if (success? rs)
	      (cons (cons 'RESULT (,r (result rs))) (rest rs))
	      rs))))

;;;
;; Simple recognizers: most of this stuff can't be used from
;;  macro definitions. Some define-s are reimplemented in the CParmacro
;;  module.
;;;

; Recognize a character using a given predicate.
(define (pchar f)
   (parser (lambda (l)
	      (let ((c (car l)))
		 (if (char? c)
		     (if (f c)
			 `((RESULT ,c) ,@(cdr l))
			 `((FAIL "char" ,c) ,@l))
		     `((FAIL "non-character" ,c) ,@l))))))


; Recognize an alphabetical character.
(define palpha
   (pchar
    (lambda (x)
       (if (char? x)
	   (or
	    (and (char>=? x #\a) (char<=? x #\z))
	    (and (char>=? x #\A) (char<=? x #\Z))) #f))))

; Recognize a decimal digit.
(define pdigit
   (pchar
    (lambda (x)
       (if (char? x)
	   (and (char>=? x #\0) (char<=? x #\9))))))

; Recognize a character range.

(define-macro (char-range f t)
   `(pchar
     (lambda (x)
	(if (char? x)
	    (and (char>=? x ,f)
		 (char<=? x ,t))))))

; A helper macro.
(define-macro (cons2 a b)
   `(list (cons ,a ,b)))

; Create a token constructor.
(define-macro (mktoken tn tf)
   `(lambda (l)
       (cons2 ,tn (,tf l))))

; Create a simple token constructor.
(define-macro (mktokenx tn)
   `(lambda (l)
       (cons2 ,tn '())))

; Convert a result of recognition to the STRING token.
(define-macro (getstring p)
   `(pR ,p (mktoken 'STRING list->string)))

; Convert a result of recognition to the NUMBER token. Could be applyed to
; any parser which recognize a valid Scheme numerical constant ('cause it is
; implemented using a simple string->number function).
; But, I'd recommend a string representation of numbers, like in the next 
; parser.
(define-macro (getnum p)
   `(pR ,p (mktoken 'NUMBER (ox string->number list->string x))))

; Parse into a plain string labeled as 'NUMERIC'
(define-macro (getsnum p)
  `(pR ,p (mktoken 'NUMERIC  list->string)))

; Recognize a specified character.
(define-macro (pcharx c)
  `(pchar (lambda (x) (char=? x ,c))))

; A nested macro expansion in a form
; (c (t sl1) (c (t sl2) ...))
(define-macro (xloopit c t sl)
  (if (null? sl) '()
      (let ((x (car sl))
	    (y (cdr sl)))
	(if (null? y)
	    `(,t ,x)
	    `(,c (,t ,x) (xloopit ,c ,t ,y))))))

; Build a string token recognizer.
; Defined for a *constant* stryng only
(define-macro (pc-string0 s)
  (let ((ss (string->list s)))
    `(xloopit p+ pcharx ,ss)))

(define-macro (pc-string s sr)
   `(pR (pc-string0 ,s)
	(mktoken 'TOKEN (lambda (x) ,sr))
	))

; Build a set of alternative token string recognizers.
(define-macro (pcs-or sl)
  `(xloopit pOR pc-string ,sl))

; Build a set of character recognizers
(define-macro (pcsx-or cl)
  `(xloopit pOR pcharx ,cl))

; Build a set of character recognizers from a character string.
(define-macro (pcsxx-or cs)
  (let ((cl (string->list cs)))
    `(pcsx-or ,cl)))


; Recognize anything but ...
(define-macro (p-but p)
   `(lambda (l)
       (let ((rs (,p l)))
	  (if (success? rs)
	      (cons (list 'FAIL "p-but") l)
	      (cons (list 'RESULT (car l)) (cdr l))
	      ))))

; Recognize any char or token. Please note that this is not wrapped in
;  a 'parser' macro. It have to succeed even on the EOF.
(define parse-any
  (lambda (l) `((RESULT) ,@l)))

; Recognize any char or token, consume it.
;
(define parse-anything
  (lambda (l) 
    (if (null? l)
	`((FAIL))
	`((RESULT) ,@(cdr l)))))

; Recognize nothing
(define parse-nop
  (parser (lambda (l) `((FAIL "expected") ,@l))))

; Recognize EOF
(define parse-eof
   (lambda (l) (if (null? l) `((RESULT)) `((FAIL "eof") ,@l))))
      
;;;
;; Common parsers. Treat this stuff as a reference: define-d functions
;;  can't be used from the macro definitions.
;;;

; Parse a number identified by regular expression
; [+-]digit*[.digit*]
(define parse-num
  (getsnum
   (p+
    (pOR (pcsx-or (#\- #\+))
	 parse-any)
    (pMANY pdigit)
    (pOR (p+ (pcharx #\.)
	     (pMANY pdigit))
	 parse-any))))

; recognize and ignore any number of spaces.
(define parse-spaces
  (pR
   (pMANY
    (pcsxx-or " \t\n"))
   (lambda (l) '())))

; Parse an alphanumerical identifier.
(define parse-ident
  (pR
   (p+
    palpha
    (pOR
     (pMANY (pOR palpha pdigit
		 (pcsxx-or "_-+*$%#@:<>|")))
     parse-any
     ))
   (mktoken 'IDENT list->string)))

; Generic lexer (tokenizer)
(define-macro (lexer . pl)
  `(pMANY (pOR ,@pl)))

; Simple lexer
(define test-lexing
  (lexer parse-spaces parse-ident parse-num))

; Token recognizer
(define-macro (ptoken t)
   `(parser (lambda (l)
	       (if (and
		    (pair? (car l))
		    (eqv? (caar l) ',t))
		   (cons (cons 'RESULT (list (car l)))
			 (cdr l))
		   (cons (cons 'FAIL (cons "tok" ',t)) l)))))

;;;
; regexp expansion - a complicated example of using parser to
; generate parsers.

; For example, the number parser could be rewritten as follows:
;;
;(define parse-numr
;   (let ((pm (pcsx-or (#\- #\+))) (dot (pcharx #\.)))
;      (getnum
;       (regexp
;	(pm / parse-any) + (pdigit *) +
;	((dot + (pdigit *)) / parse-any)))))

; Or, even better:
;;
;(define parse-numr2
;   (regexp
;    (
;     ((#\- / #\+) / parse-any) + (pdigit *) +
;      (("." + (pdigit *)) / parse-any))
;    -> (mktoken 'XNUMBER (oxx string->number list->string))
;    ))

; A helper macro - symbol recognizer. (FIXME!)
(define-macro (psym s)
   `(parser (lambda (l)
	       (let ((q ',s))
		  (if (eqv? (car l) q)
		      `((RESULT ,q) ,@(cdr l))
		      `((FAIL ("symbol" ,q ,(car l))) ,@l))))))


; A helper macros
(define-macro (ret v)
   (let ((s (gensym)))
      `(lambda (,s) list ,v)))

(define-macro (retlc v)
   `(ret (list (cdr ,v))))

(define-macro (aprs p)
   `(oxx result ,p car))

(define-macro (mlog v)
   `(let ((fl (append-output-file "./scm-log")))
       (write ,v fl)
       (close-output-port fl)))

; A regexp expander definition. It redefines a set of previously defined
;  simple recognizers, but YOU have to use the stuff defined in the
;  CParmacro module.

(define-macro (regexp . l)
   (letrec
	 ((rgxpp
	   (let* ((plist 
		   (parser
		    (lambda (l) (if (list? (car l))
				    `((RESULT ,(car l)) ,@(cdr l))
				    `((FAIL ("list" ,(car l))) ,@l)))))
		  (pchar (lambda (f)
			    (parser (lambda (l)
				       (let ((c (car l)))
					  (if (char? c)
					      (if (f c)
						  `((RESULT ,c) ,@(cdr l))
						  `((FAIL "char" ,c) ,@l))
					      `((FAIL "non-character" ,c)
						,@l)))))))
		  (pdigit
		   (pchar
		    (lambda (x)
		       (if (char? x)
			   (and (char>=? x #\0) (char<=? x #\9))))))
		  (pchr 
		   (parser
		    (lambda (l) (if (char? (car l))
				    `((RESULT ,(car l)) ,@(cdr l))
				    `((FAIL ("char" ,(car l))) ,@l)))))
		  (psymbl
		   (parser
		    (lambda (l) (if (symbol? (car l))
				    `((RESULT ,(car l)) ,@(cdr l))
				    `((FAIL ("symbol" ,(car l))) ,@l)))))
		  (pstrg 
		   (parser
		    (lambda (l) (if (string? (car l))
				    `((RESULT ,(car l)) ,@(cdr l))
				    `((FAIL ("string" ,(car l))) ,@l)))))
		  (parse-all
		   (parser (lambda (l) `((RESULT ,(car l)) ,@(cdr l)))))
		  (identity (lambda (x) x))
		  (plistscm
		   (parser (lambda (l) (if (and (list? (car l))
						(eqv? 'SCM (caar l)))
					   `((RESULT ,(cdar l)) ,@(cdr l))
					   `((FAIL ("scmlist" ,(car l)))
					     ,@l)))))
		  (plisttkn
		   (parser (lambda (l) (if (and (pair? (car l))
						(and
						 (symbol? (caar l))
						 (null? (cdar l))
						))
					   `((RESULT ,(caar l)) ,@(cdr l))
					   `((FAIL ("tknlist" ,(car l)))
					     ,@l)))))
		  (subst0
		   (lambda (x z)
		      (letrec
			    ((subst
			      (lambda (l n)
				 (if (symbol? l)
				     (let*
					   ((tt
					     ((oxx string->list
						   symbol->string) l))
					    (tz
					     (
					      (pOR
					       (pR
						(p+ (pcharx #\$)
						    (pMANY pdigit))
						(lambda (l)
						   (string->number
						    (list->string (cdr l)))))
					       (pR
						(p+ (pcharx #\$)
						    (pcharx #\$)
						    (pMANY pdigit))
						(lambda (l)
						   (list
						    (string->number
						     (list->string (cddr l)))))
						))
					      tt
					      ))
					    )
					(if (success? tz)
					    (begin
					       (if (list? (cdar tz))
						   `(cdr
						     (list-ref
						      ,n
						      ,(car (cdar tz))))
						   `(list-ref ,n ,(cdar tz)))
					       )
					    l))
				     (if (pair? l)
					 (cons
					  (subst (car l) n)
					  (subst (cdr l) n))
					 l)))))
			 (let* ((n (gensym))
				(v (subst x n))
				)
			    (if z
				`(lambda (,n)
				    (,v ,n))
				`(lambda (,n) ,v)
				)
			    )
			 )))
		  (body
		   (pOR
		    plistscm
		    (pR plisttkn (lambda (l)
				    `((ptoken ,(car l)))))
		    ;; List recognition
		    (pR plist (oxx  cdar rgxpp car))
		    ;; Single character recognizer
		    (pR pchr (lambda (l) `((pcharx ,(car l)))))
		    ;; String sequental recognizer
		    (pR pstrg (lambda (l) `((pc-string0 ,(car l)))))
		    ;; All other possible tokens (any symbol)
		    (pR psymbl identity)
		    ;; FIXME: we have to add a number recognizer
		    ;; (character code), ...
		    ))
		  )
	      (pOR
	       ;; [parser ^] syntax: ignore
	       (pR (p+ body (psym ^))
		   (lambda (l)
		      `((pR ,(list-ref l 0) (lambda (l) '())))))
	       ;; [parser *] syntax: one-or-more recognizer
	       (pR (p+ body (psym *))
		   (lambda (l)
		      `((pMANY ,(list-ref l 0)))))
	       ;; [parser =] syntax: one-or-nothing recognizer
	       (pR (p+ body (psym =))
		   (lambda (l)
		      `((pOR ,(list-ref l 0) parse-any))))
	       ;; [parser !] syntax: anything but
	       (pR (p+ body (psym !))
		   (lambda (l)
		      `((p-but ,(list-ref l 0)))))
	       ;; [parser **] syntax: ignore again
	       (pR (p+ body (psym **))
		   (lambda (l)
		      `((pR ,(list-ref l 0) (lambda (l) '())))))
	       ;; [char - char] syntax: char range recognizer
	       (pR (p+ pchr (psym -) pchr)
		   (lambda (l)
		      `((char-range ,(list-ref l 0) ,(list-ref l 2)))))
	       ;; [$ "chars"] syntax: oneofchar selecter
	       (pR (p+ (psym $) pstrg)
		   (lambda (l)
		      `((pcsxx-or ,(list-ref l 1)))))
	       ;; [parser + parser] syntax: sequence recognizer
	       (pR (p+ body (psym +) rgxpp)
		   (lambda (l)
		      `((p+ ,(list-ref l 0)
			    ,(list-ref l 2)))))
	       ;; [parser / parser] syntax: alternative selecter
	       (pR (p+ body (psym /) rgxpp)
		   (lambda (l)
		      `((pOR ,(list-ref l 0)
			     ,(list-ref l 2)))))
	       ;; [parser /* parser] syntax: right priority
	       (pR (p+ body (psym /*) rgxpp)
		   (lambda (l)
		      `((pORMANY ,(list-ref l 0)
				 ,(list-ref l 2)))))
	       ;; [parser */ parser] syntax: right priority - more readable
	       (pR (p+ body (psym */) rgxpp)
		   (lambda (l)
		      `((pORMANY ,(list-ref l 2)
				 ,(list-ref l 0)))))
	       ;; [parser -> lambda] syntax: result application
	       (pR (p+ body (psym ->) plist)
		   (lambda (l)
		      `((pR ,(list-ref l 0)
			    ,(subst0 (list-ref l 2) #t)))))
	       ;; [parser :-> lambda] syntax: shielded result application
	       (pR (p+ body (psym :->) plist)
		   (lambda (l)
		      `((pR ,(list-ref l 0)
			    ,(subst0 (list-ref l 2) #f)))))
	       ;; Trivials and lists.
	       body))))
      (let ((lp ((parser rgxpp) l)))
	 (if (and (success? lp) (null? (rest lp)))
	     (car (result lp))
	     (begin
		(display "\n\nregexp expansion error: ")
		(display l)
		(display ";;;\n")
		identity)))))
