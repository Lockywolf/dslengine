
(defun prgraph (e l)
  (cond
   ((null l) ())
   ((numberp l) l)
   (T
    (let ((tt (gethash l e)))
      (if tt
          `(ref ,tt)
          (let ((g (gethash 'CNT e)))
            (setf (gethash l e) (+ g 0))
            (setf (gethash 'CNT e) (+ g 1))
            (if (listp l)
                (cons (prgraph e (car l)) (prgraph e (cdr l)))
                (if (and (vectorp l) (not (stringp l)))
                    (let* ((n (length l))
                           (vc (make-array n))
                           )
                      (labels ((fil (i)
                                    (if (< i n)
                                        (progn 
                                          (setf (aref vc i) (prgraph e (aref l i)))
                                          (fil (+ i 1))))))
                        (fil 0))
                      vc)
                    l))))))))

(defun pgraph (l)
  (let ((h (make-hash-table)))
    (setf (gethash 'CNT h) 0)
    (prgraph h l)))

(defun is-const (l)
  (or (not (listp l))
      (eq (car l) 'quote)))

(defun iscar (x v)
  (if (listp x)
      (eq v (car x))
    NIL))

(defconstant combI '**I)
(defconstant combS '**S)
(defconstant combK '**K)
(defconstant combB '**B)
(defconstant combC '**C)
(defconstant combY '**Y)
(defconstant combQuote '**Q)

(defconstant combQuot (vector combQuote 1))
(defconstant xxcons '**cons)

(defun Xabs (v l)
  (cond
   ((eq v l) combI)
   ((is-const l)
    `(,combK . ,l))
   ((listp l)
    (if (null (cdr l))
	`(,(Xabs v (car l)))
      (let ((t1 (Xabs v (car l)))
	    (t2 (Xabs v (cdr l))))
	(if (iscar  t1 combK)
	    (if (eq t2 combI)
		(cdr t1)
	      (if (iscar t2 combK)
		  `(,combK . ( ,(cdr t1) . ,(cdr t2)))
		`((,combB . ,(cdr t1)) . ,t2)))
	  (if (iscar t2 combK)
	      `((,combC . ,t1) . ,(cdr t2))
	    `((,combS . ,t1) . ,t2))
	  ))))
   (T 'error)))

(defun comb (l)
  (cond
   ((is-const l) l)
   ((vectorp l) l) 
   ((and (listp l)
	 (eq (car l) 'lambda))
    (Xabs (cadr l) (comb (cddr l))))
   ((listp l)
    (if (null (cdr l)) (comb (car l))
      (cons (comb (car l)) (comb (cdr l)))))
   (T 'error)))

(defun curryfy (l)
  (cond
   ((null l) l)
   ((is-const l) l)
   ((listp l)
    (if (eq (car l) 'lambda)
	(labels
	 ((lop (x)
	   (if (null x) (mapcar #'curryfy (cddr l))
	     `(lambda ,(car x) ,(lop (cdr x))))))
	 (lop (cadr l)))
      (labels
       ((lop (f x)
	   (if (null x) f
	     (let ((n (cdr x)))
	       (if (null n)
		   (cons f (curryfy (car x)))
		 (lop (cons f (curryfy (car x))) n))))))
       (lop (curryfy (car l)) (cdr l)))))
   (T l)))

(defconstant functs `((+ ,#'+ 2) (*  ,#'* 2) (/ ,#'/ 2)
		      (- ,#'- 2) (> ,#'> 2)))

(defvar fns
  (let ((a (make-hash-table)))
    (mapcar #'(lambda (h)
		(setf (gethash (car h) a) (cadr h))) functs)
    a))

(defconstant arity-tab
  `((,combI . 1)
    (,combK . 2)
    (,combS . 3)
    (,combB . 3)
    (,combC . 3)
    (,combY . 1)
    (,combQuote . 1)
    (if . 3)
    (cons . 2)
    ))

(defvar ars
  (let ((a (make-hash-table)))
    (mapcar #'(lambda (h)
	      (setf (gethash (car h) a) (cdr h))) arity-tab)
    (mapcar #'(lambda (h)
		(setf (gethash (cadr h) a) (caddr h))) functs)
    a))

(defvar environ (make-hash-table))

(defun newcomb (n a f)
  (setf (gethash f ars) a)
  (setf (gethash n fns) f))

(defun arity (x)
  (let ((r (gethash x ars)))
    (if r r 1)))

(defun gcompx (l)
  (cond
   ((null l) l)
   ((listp l)
    (if (is-const l) (cadr l)
      (cons (gcompx (car l)) (gcompx (cdr l)))))
   ((symbolp l)
    (let ((t0 (gethash l environ)))
      (if t0 t0
	(let ((tt (gethash l fns)))
	  (if tt (vector tt (arity tt)) (vector l (arity l)))))))
   (T l)
   ))

(defvar specials (make-hash-table))
(defun add-special (n f u)
  (setf (gethash n specials) (cons f u)))

(add-special 'quote #'(lambda (x) x) NIL)

(defun unroll (l)
  (cond
   ((null l) l)
   ((listp l)
    (if (symbolp (car l))
	(let ((h (gethash (car l) specials)))
	  (if h
	      (let ((f (car h))
		    (u (cdr h)))
		(let ((r (funcall f l)))
		  (if u (unroll r) r)))
	    (cons (unroll (car l)) (unroll (cdr l)))))
      (cons (unroll (car l)) (unroll (cdr l)))))
   (T l)))

(defun s2g (l)
  (gcompx (comb (curryfy (unroll l)))))

(defun newenvr (n v)
  (setf (gethash n environ) v))

(defun newenvrx (n v) (newenvr n (s2g v)))

(newenvr 'NL 'NL)

(defun ntht (n l)
  (if (> n 0) (ntht (- n 1) (cdr l)) l))


(defvar top '())


(defun gr-car (l)
  (gr #'(lambda (v)
	    (setf (car l) v)
	    (gr-car l)
	    (car l))
      (car l))
  (car l))

(defun gr-cdr (l)
  (gr #'(lambda (v)
         (setf (cdr l) v)
         (gr-cdr l)
         (cdr l))
      (cdr l))
  (cdr l))

(defun gr (cf l)
  (if (listp l) (grt cf l) l))


(defun grt (cf l)
  (let*
      ((redex '())
       (func '())
       (rxcf '())
       (dipth 0)
       (arit 0)
       (path '())
       (args '())
       (rxcfz #'(lambda (v)
		  (funcall rxcf v)
		  (if (eq cf rxcf) NIL
		    (gr cf l))
		  v)))
    (labels
	((loap (c)
	  (setf path (cons c path))
	  (if (vectorp (car c))
	      (let*
		  ((cc (car c))
		   (ar (aref cc 1)))
		(if (eq cc xxcons) '()
		  (progn
		    (setf arit ar)
		    (setf func (aref cc 0))
		    (if (>= (+ 1 dipth) arit)
			(progn
			  (let ((x (ntht (- ar 1) path)))
			    (setf redex (car x))
			    (if (null (cdr x))
				(setq rxcf cf)
			      (setf rxcf
				    (let ((tt (cadr x)))
				      #'(lambda (v)
					  (setf (car tt) v)
					  v)))))
			  (setf args
				(labels ((lop2 (z i)
					       (if (< i 0) '()
						 (cons (car z)
						       (lop2 (cdr z)
							     (- i 1))))))
				  (lop2 path (- arit 1))))
			  )))))
	    (if (listp c)
		(if (null c) c
		  (progn
		   (setf dipth (+ dipth 1))
		   (loap (car c))))))))
      (loap l))
    (if (null args) args
      (cond
       
       ((functionp func)
	(let ((m (mapcar #'(lambda (x)
			     (gr-cdr x)) args)))
	  (funcall rxcfz (apply func m))))

       ((eq func combI)
	(funcall rxcfz (cdar args)))
       ((eq func combK)
	(funcall rxcfz (cdadr args)))
       ((eq func combS)
	(let ((f (cdar args))
	      (g (cdadr args))
	      (x (cdaddr args)))
	  (funcall rxcfz (cons (cons f x) (cons g x)))))
       ((eq func combB)
	(let ((f (cdar args))
	      (g (cdadr args))
	      (x (cdaddr args)))
	  (funcall rxcfz (cons f (cons g x)))))
       ((eq func combC)
	(let ((f (cdar args))
	      (g (cdadr args))
	      (x (cdaddr args)))
	  (funcall rxcfz (cons (cons f x) g))))
       
       ((eq func combY)
	(let ((tt (cons (cdar args) '())))
	  (setf (cdr tt) tt)
	  (funcall rxcfz tt)))

       ((eq func 'if)
	(let ((f (gr-cdr (car args)))
	      (g (cdadr args))
	      (h (cdaddr args)))
	  (if (eq f T)
	      (funcall rxcfz g)
	    (funcall rxcfz h))))

       ((eq func 'cons)
	(let ((f (cdar args))
	      (g (cdadr args)))
	  (funcall rxcf (vector xxcons f g))))
       
       ((numberp func)
	(let ((f (gr-cdr (car args))))
	  (if (vectorp f)
	      (let ((r (aref f func)))
		(funcall rxcfz
			 (gr #'(lambda (v)
				 (setf (aref f func) v)
				 v
				 ) r))))))
       (T 'error)
       )))
  l)

(newenvr 'car (vector 1 1))
(newenvr 'cdr (vector 2 1))

(defun tst (l)
  (let* ((s (s2g l))
	 (tt (list s))
	 (r (gr-car tt)))
    (print `("SRC:" ,l))
    (print `("RES:" ,(pgraph r)))
  '()
  ))

(newcomb 'llcons ; low level cons
	 2
	 #'cons
	 )

(newcomb 'isnull
	 1
	 #'null)

(defun iscons (v)
  (and (vectorp v)
       (eq xxcons (aref v 0))))

(defun g2l (l)
  (if (or (null l) (eq 'NL l))
      '()
    (if (iscons l)
	(let ((ca (aref l 1))
	      (cr (aref l 2)))
	  (cons (g2l (gr-car (list ca))) (g2l (gr-car (list cr)))))
      l)))

(defun l2g (l)
  (cond
   ((null l) 'NL)
   ((listp l)
    (vector xxcons (l2g (car l)) (l2g (cdr l))))
   (T l)))

(add-special
 'let
 #'(lambda (v)
     (if (listp (cadr v))
	 (let ((args (mapcar #'car (cadr v)))
	       (defs (mapcar #'cadr (cadr v))))
	   `((lambda ,args ,(caddr v)) ,@defs))
       ;; else: recursive let.
       (let ((name (cadr v))
	     (args (mapcar #'car (caddr v)))
	     (defs (mapcar #'cadr (caddr v))))
	 `((,combY (lambda (,name ,@args) ,(cadddr v))) ,@defs))))
 T)

(add-special
 'letrec
 #'(lambda (v)
     `(let ,(mapcar #'(lambda (x)
			 (let ((nm (car x)))
			 `(,nm (,combY (lambda (,nm) ,(cdr x))))))
		     (cadr v))
	,(caddr v)))
 T)

(defmacro lazy (l)
  (let* ((c (s2g l))
	 (z (list c)))
    `(g2l (gr-car ',z))))

(defmacro l-def (nm args body)
  (let ((c (s2g `(lambda ,args ,body))))
    `(newenvr ',nm ',c)))

(defmacro l-defrec (nm args body)
  (let ((c (s2g `(,combY (lambda (,nm ,@args) ,body)))))
    `(newenvr ',nm ',c)))

(defmacro l-prim (nm args . body)
  (let ((ar (length args)))
    `(newcomb ',nm ,ar #'(lambda ,args ,@body))))

(l-defrec
 from (i)
 (cons i (from (+ i 1))))

(l-prim
 null (x)
 (eq x 'NL))

(l-defrec
 map (f x)
 (if (null x) NL
   (cons (f (car x)) (map f (cdr x)))))

(l-defrec
 cut (n x)
 (if (> n 0) (cons (car x) (cut (- n 1) (cdr x)))
   NL))

;; This could be used to extend the Intermediate Lambda language.
(defmacro l-defmacro (nm args body)
  (let* ((x (s2g `(lambda ,args ,body)))
	 (zz #'(lambda (l)
		 (let* ((l2 (l2g l))
			(rs (gr-car (list (cons x l2))))
			(xrs (g2l rs)))
		   xrs))))
    `(add-special ',nm ,zz T)))

;; Real Lisp macros
(defmacro l-defhlmacro (nm args body)
  (let* ((x (s2g `(lambda ,args ,body)))
	 (zz #'(lambda (l)
		 (let* ((l2 (l2g l))
			(rs (gr-car (list (cons x l2))))
			(xrs (g2l rs)))
		   xrs)))
	 (b (gensym))
	 (a (gensym))
	 )
    `(defmacro ,nm (&rest ,a)
       (funcall ,zz ,a))))

(l-defmacro list (x)
	    (let ((t (cdr x)))
	      (if (null t) NL
		(cons 'cons (cons (car t) (cons (cons 'list (cdr t)) NL))))))

;; Lazy with parameters: how to connect two diferent worlds.
(defmacro plazy (l . a)
  (let* ((c (s2g l))
	 (f (gensym))
	 (g (gensym))
	 )
    `(funcall
      #'(lambda (&rest ,f)
	  (g2l (gr-car (list
			(cons
			      (s2g (list 'lambda (list ',g)
					 (cons ',g (mapcar #'l2g ,f))))
			      ',c)
			)))
	  )
      ,@a)
    ))

;Example:
;(print (lazy ((lambda (a b) (+ (* 10 a) b)) 2 2)))
;(print (plazy (lambda (a b) (+ (* 10 a) b)) (* 2 1) 2))

(add-special
 'get
 #'(lambda (l)
     (let ((n (cadr l)))
       (if (null (cddr l)) (vector (+ n 1) 1)
	 (cons (vector (+ n 1) 1) (cddr l)))))
 T
 )

(l-prim prn (x)
	(print (pgraph x))
	x
	)

(l-prim cons3 (x1 x2 x3)
	(vector xxcons x1 x2 x3))

(l-prim cons4 (x1 x2 x3 x4)
	(vector xxcons x1 x2 x3 x4))

(l-prim cons5 (x1 x2 x3 x4 x5)
	(vector xxcons x1 x2 x3 x4 x5))

(tst '(get 0 (cons 1 2)))

(quote
(lazy (let lop ((l (from 1))
		(i 0))
	   (if (> 3000 i)
	       (cons (prn (car l)) (lop (cdr l) (+ i 1)))
	     NL)))

)