;;
;; The stuff defined in CParsing.scm redefined
;;  specially for macro expanders
;;

(define num
   (parser
    (lambda (x)
       (if (number? (car x))
	   `((RESULT ,(car x)) ,@(cdr x))
	   `((FAIL) ,@x)))))

(define lst
   (parser
    (lambda (x)
       (if (pair? (car x))
	   `((RESULT ,(car x)) ,@(cdr x))
	   `((FAIL) ,@x)))))

(define-macro (defparsers v)
   v)