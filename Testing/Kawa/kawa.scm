(define GENSYM-COUNT 0)

(define (gensym)
   (let ((s (string->symbol (string-append "gensym-symbol-" (number->string GENSYM-COUNT)))))
      (set! GENSYM-COUNT (+ GENSYM-COUNT 1))
      s))

(defmacro define-macro (args . form) `(defmacro ,(car args) ,(cdr args) ,@form))

(define-macro (print . v)
   `(begin
       (display ,@v)
       (display "\n")))
   
