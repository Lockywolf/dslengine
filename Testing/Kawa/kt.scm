   (load "./kawa.scm")
   (load "./CParsing.scm")
   (load "./CParmacro.scm")

;; Wow!!! 3rd-level macro, using regexp to define a macro-expander!!!
(define-macro (exp1 . v)
   (defparsers
      (letrec
	    ((epr
	      (let
		    ((body
		      (regexp
		       (num :-> $0)
		       /
		       (lst -> (aprs epr)))))
		 (regexp
		  ((body + (SCM psym +) + epr)
		   :-> (list (+ $0 $2)))
		  /
		  ((body + (SCM psym -) + epr)
		   :-> (list (- $0 $2)))
		  /
		  ((body + (SCM psym *) + epr)
		   :-> (list (* $0 $2)))
		  /
		  ((body + (SCM psym /) + epr)
		   :-> (list (/ $0 $2)))
		  / body
		  ))))
	 (print (epr v))
	 (car (result (epr v))))))

(define (main argv)
   (print (exp1 2 + ((10 * 5) / 4))) ;; "Partial evaluation", ha-ha!
   )

;;;------------

(main '())